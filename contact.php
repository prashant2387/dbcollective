<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script type="text/javascript" src="js/jquery.label_better.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title>Contact</title>

</head>
<body>
    <div class="outer-cont">
	<div class="wrapper" id="about">
	<header>
		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
			<?php include("header.php"); ?>
		</div>
	</header>
	
	<section class="case">
		<div class="case-title">
			<h1 class="db-font case-s">Send us <br>a message!</h1>
		</div>	
		<div class="container">
			<div class="inner-container">
				<div class="contact">
					<form method="post">
						<div class="pull-left t-area">
						<span class="text-hi" id="aboveText">Hi,</span>
						<div class="resize">
						<textarea id="contactarea" class="input" name="description" color="#000" required autofocus ></textarea>
                        </div>						
					
						<div class="in-type-fields">
						    <input placeholder="Full Name *"  class="invt-contact b-radius" type="text" name="fullname" required >
							
							<input placeholder="Email *" class="invt-contact" type="email" name="email" required>
							
							<input placeholder="Phone Number *"  class="invt-contact phn" type="text" name="phone" required >
							
							<input type="submit" name="submit" value="contact us" class="contact-btn">
						</div>
						
						</div>
						<!--<div class="pull-right contact-form">
						<div class="input-star"><input type="text" placeholder="Full Name" name="fullname" class="input" color="#000" required /> <span>*</span></div>
						
						<div class="input-star"><input type="email"  placeholder="Email"  name="email" class="input" color="#000" required /> <span>*</span></div>
						
						<div class="input-star"><input type="text" placeholder="Phone Number" name="phone" class="input" color="#000" pattern="[0-9]+" title="only 0-9 digits" required /> <span>*</span></div>
						
						<input type="submit" name="submit" value="Send" class="btn-send">
						</div> -->
					</form>
					<?php
						if(isset($_POST['submit'])){
							$description = $_POST['description'];
							$fullname = $_POST['fullname'];
							$email = $_POST['email'];
							$phone = $_POST['phone'];
							
							//$to = "design@dbcollective.co";
							$to = "rajesh.kumar@trigma.co.in";
							$subject = $fullname." has contacted you";
							$from = $email;
						
						
							$message = "<html>";
							$message .= "<head>
							<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
							<title>email-template</title>
							<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
							</head>";
							$message .= "<body style='margin:0; padding:0; font-family:helvetica'>";
							$message .= " <table style='width:100%; max-width:600px; margin:0 auto;' border='1' cellpadding='0' cellspacing='0'><tr><td>";
							$message .= " <table width='100%' cellpadding='0' cellspacing='0'>
												  <tr>
													  <td style='padding-left:8px; background:#000000; color:#ffffff; padding-top:10px; padding-bottom:6px; border-bottom:25px solid #000000; text-align:center;'>You have received a new message from the contact form on your website</td>
												  </tr>	  
											</table>";
								$message .= "<table width='100%' style='padding:0 10px;'>
								 <tr>
									  <td>	
										<p style='font-size:12px;'><strong>Name : </strong>".$fullname."</p>											  
										 <p style='font-size:12px;'><strong>Email : </strong>".$email."</p>										 
										 <p style='font-size:12px;'><strong>Phone : </strong> ".$phone."</p>
										 <p style='margin-bottom:40px;font-size:12px;'><strong>Description :</strong> ".$description."</p>
									  </td>

								  </tr>
							</table>";
							$message .= "</td></tr></table></body></html>";							
							$headers ="MIME-Version: 1.0" . "\r\n";
							$headers .="Content-type:text/html;charset=iso-8859-1" . "\r\n";
							$headers .="From: $from" . "\r\n";

							
							$mail = mail($to, $subject, $message, $headers);
							
							if($mail){ echo "<p class='contact_success'> <span>Thankyou!</span> Your message sent successfully.</p>"; }
							else{ echo "<p class='contact_error'>Sorry! Some error occured.</p>"; }
						}
					
					
					?>
					

				</div>
			</div>
		</div>
	</section>
		<div class="clear"></div>
		</div>
	
	
	
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	
		<div class="clear"></div>
	</div>
 <script>
		$(document).ready(function(){
			 $('.bxslider').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 8,
		maxSlides: 8,
		
		slideWidth: 0,

			  mode: 'vertical',
			  responsive: true
			});
			//Menu Slide Js
	jQuery(document).ready(function($) {
		$('.smobitrigger').smplmnu();
		});
		});
		$(document).ready( function() {
	    $(".label_better").label_better({
	      easing: "bounce"
	    });
		
		$("#contactarea").keydown(function() {
			$("#aboveText").addClass("darkText");
		});

		$('#contactarea').focusout(function (){
			if (!$("#contactarea").val()) {
				$("#aboveText").removeClass("darkText");				
			}
		});
		
	  });
</script>
	
</body>
</html>