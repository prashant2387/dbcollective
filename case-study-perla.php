<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title> Case Study Perla </title> 

</head>
<body>
	<div class="wrapper">
	<header>
		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation" id="ballon">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
			<?php include("header.php"); ?>
		</div>
	</header>
	<div class="slider-main perla-slider">
		<div class="slider-text">
			<h1>Perla Dolce</h1>
			<div class="switch-down">
				<a href="#switch"><img src="images/switch-down.png"/></a>
				<a href="#" class="see-project">See the project</a>
			</div>
		</div>	
	</div>
	<section>
		<div class="container" id="switch">
			<div class="inner-container">
				<div class="info client-david att-t">
					<div class="about">
						<h2>Balloon Baskets</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</p>
					</div>
				</div>
				<div class="info addresss client-project case-at">
					<div class="project-p">
						<h6>Project</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="project-p">
						<h6>Year</h6>
						<p><b>2016</b></p>
					</div>
					<a href="#">Go to the website <img src="images/right-arrow.png"/></a>
				</div>
			</div>
		</div>
	</section>
	<section class="extra-info consulting">
		<div class="inner-container">
				<div class="info info-inner">
					<div class="about">
						<h2>Consulting</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,</p>
					</div>	
				</div>
				<div class="info addresss client-project case-at info-right">
					<ul>
						<li><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</span></li>
						<li><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</span></li>
						<li><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</span></li>
						<li><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</span></li>
						<li><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</span></li>
					</ul>
				</div>
			</div>
	</section>	
	<section class="food-content" id="ballon-basket">
		<div class="food-section bxslider3">
			<div class="food-first-section">
			<div class="food-first-section perlaa-img"></div>
				<div class="food-text baln-text">
				<h2>Collateral</h2>
				<p class="baln-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
				</div>
			</div>
			<div class="food-first-section">
			<div class="food-first-section collateral"></div>
				<div class="food-text baln-text">
				<h2>Collateral</h2>
				<p class="baln-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
				</div>
			</div>
			<div class="food-first-section">
			<div class="food-first-section branding"></div>
				<div class="food-text baln-text">
				<h2>Branding</h2>
				<p class="baln-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
				</div>
			</div>
			<div class="food-first-section">
			<div class="food-first-section pakaging"></div>
				<div class="food-text baln-text">
				<h2>Pakaging</h2>
				<p class="baln-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
				</div>
			</div>
			<div class="food-first-section">
			<div class="food-first-section pakagingg"></div>
				<div class="food-text baln-text">
				<h2>Pakaging</h2>
				<p class="baln-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
				</div>
			</div>
			<div class="food-first-section">
			<div class="food-first-section pakaginggg"></div>
				<div class="food-text baln-text">
				<h2>Pakaging</h2>
				<p class="baln-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
				</div>
			</div>
		</div>
	</section>
	<section class="perla-down-section">
		<div class="client-imgs web-set">
			<img src="images/perlaa.jpg"/>
		</div>
	</section>
	<section>
		<div class="inner-container">	
			<div class="sub-container">
				<ul>
					<li><img src="images/perla-01.jpg"/></li>
					<li><img src="images/perla-02.jpg"/></li>
					<li><img src="images/perla-03.jpg"/></li>
					<li><img src="images/perla-04.jpg"/></li>
				</ul>
			</div>
		</div>	
	</section>
	<section class="perla-bg">
		<div class="inner-container">
			<div class="perla-insta">
				<div class="insta-para">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
					<p>laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est</p>
				</div>
				<img src="images/insta-image.png"/>
			</div>
			<div class="perla-tags">
				<ul>
				<li><a href="#">Perla's Instagram <img src="images/arrow-white.png"/></a></li>
				<li><a href="#">Buy it Now <img src="images/arrow-white.png"/></a></li>
			</div>
		</div>
	</section>
	<section>
		<div class="inner-container">
			<div class="bottom-links">
				<a href="case-study-perla.php"><img src="images/left-arrow.png"/> Perla Dolce</a>
				<a href="case-study-at&t.php">At&t <img src="images/arrow-right.png"/></a>
			</div>
		</div>
	</section>
	
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	
	
	</div>
 <script>
		$(document).ready(function(){
			 $('.bxslider3').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  mode: 'vertical',
			  responsive: true
			});
			//Menu Slide Js
		jQuery(document).ready(function($) {
			$('.smobitrigger').smplmnu();
			});
			
			jQuery('a[href^="#"]').click(function(e) {

			jQuery('html,body').animate({ scrollTop: jQuery(this.hash).offset().top}, 1000);

			return false;

			e.preventDefault();

		});


		
		});
</script>
	
</body>
</html>