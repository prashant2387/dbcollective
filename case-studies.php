<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title>Case Studies</title>

</head>
<body>
	<div class="wrapper" id="about">
	<header>

		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
					<?php include("header.php"); ?>
		</div>
	</header>
	
	<section class="case">
		<div class="case-title">
			<h1 class="db-font case-s">Work / <span>Case Studies</span></h1>
			<a href="work.php" class="back-arrow"><img src="images/back-arrow.png"/></a>
		</div>	
		<div class="container grid-four">
			<div class="inner-section">
				<div class="perlaa">
					<img src="images/perla-case.jpg"/>
					<a href="case-study-perla.php">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
				<div class="umjaa">
					<img src="images/umja-case.jpg"/>
					<a href="case-study-umoja.php">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
			</div>
			<div class="inner-section gainss">
				<img src="images/gains.jpg"/>
				<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
			</div>
			<div class="inner-section">
				<div class="perlaa">
					<img src="images/eat.jpg"/>
					<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
				<div class="umjaa">
					<img src="images/david.jpg"/>
					<a href="clients.php">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
			</div>
		</div>
		<div class="container grid-four">
			<div class="inner-section">
				<div class="perlaa">
					<img src="images/cardiac.jpg"/>
					<a href="case-study-CardiacSpecialists.php">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
			</div>
			<div class="inner-section">
				<div class="perlaa">
					<img src="images/balloon.jpg"/>
					<a href="case-study-ballon.php">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
				<div class="umjaa">
					<img src="images/mbsi.jpg"/>
					<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
			</div>
			<div class="inner-section">
				<div class="perlaa">
					<img src="images/uniform.jpg"/>
					<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
				<div class="umjaa">
					<img src="images/memorial.jpg"/>
					<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
			</div>
		</div>
	</section>
	
	
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	
	</div>
 <script>
		$(document).ready(function(){
			 $('.bxslider').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 8,
		maxSlides: 8,
		
		slideWidth: 0,

			  mode: 'fade',
			  responsive: true
			});
			//Menu Slide Js
	jQuery(document).ready(function($) {
		$('.smobitrigger').smplmnu();
		});
		});
</script>
	
</body>
</html>
