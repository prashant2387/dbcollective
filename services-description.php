<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script type="text/javascript" src="js/jquery.label_better.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title>Services Description</title>

</head>
<body>
	<div class="wrapper" id="about">
	<header>
		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
	<?php include("header.php"); ?>
		</div>
	</header>
	
	<section class="case service-dev">
		<div class="case-title">
			<h1 class="db-font case-s">Services / <a href="#" class="business-dev">Business Development </a><a href="#"> / Business Consulting</a></h1>
		</div>	
		<div class="container">
			<div class="inner-container">
				<div class="pull-left strategy">
					<h1>What makes our<br> business strategy so great?</h1>
					<div class="content-section">
						<h4>Get your business off the ground!</h4>
						<p>Our team has been involved as a founder or investor in over 33 companies. We've been through all the stages. Whether it's business plans, fund raising, product building, or exit strategies, we can help.</p>
					</div>
					<div class="content-section">
						<h4>Present your business the right way</h4>
						<p>Market well, design well, sell well. These are easy concepts to hear, but harder to put into practice. You need to look like the million dollar company you represent. That starts with attention to detail at every level.</p>
					</div>
					<div class="content-section">
						<h4>Represent yourself honestly</h4>
						<p>Consumers are smarter than ever. They've seen the companies that say one thing and do another. Your company should have a great culture initiated, and honest campaigns shared with their consumers. Be the brand you want your consumers to think you are.</p>
					</div>
				</div>
				<div class="contact pull-right service-contact">
					<h1>Let us help<br> your Business!</h1>
					<form>
						<div class="pull-right contact-form">
						<div class="input-star"><input type="text" onblur="if(this.value=='')this.value='Full Name';" onfocus="if(this.value=='Full Name')this.value='';" value="Full Name" class="input" color="#000"> <span>*</span></div>
						<div class="input-star"><input type="text" onblur="if(this.value=='')this.value='Email';" onfocus="if(this.value=='Email')this.value='';" value="Email" class="input" color="#000"> <span>*</span></div>
						<div class="input-star"><input type="text" onblur="if(this.value=='')this.value='Phone Number';" onfocus="if(this.value=='Phone Number')this.value='';" value="Phone Number" class="input" color="#000"> <span>*</span></div>
						<div class="input-star"><textarea type="text" onblur="if(this.value=='')this.value='Description';" onfocus="if(this.value=='Description')this.value='';" value="Description" class="input" color="#000">Description</textarea> <span>*</span></div>
						<button class="btn-send">Send</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	
	</div>
 <script>
		$(document).ready(function(){
			 $('.bxslider').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 8,
		maxSlides: 8,
		
		slideWidth: 0,

			  mode: 'vertical',
			  responsive: true
			});
			//Menu Slide Js
	jQuery(document).ready(function($) {
		$('.smobitrigger').smplmnu();
		});
		});
		$(document).ready( function() {
	    $(".label_better").label_better({
	      easing: "bounce"
	    });
	  });
</script>
	
</body>
</html>
