<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title>DB Collective</title>

</head>
<body>
	<div class="wrapper" id="about">
	<header>

		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
					<?php include("header.php"); ?>
		</div>
	</header>
	<section>
		<h1 class="db-font abt-font">We are DB Collective :)</h1>
		<!-- <div class="inner-container userss">
			<ul>
				<li>
					<img src="images/user1.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user2.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user3.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user4.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user5.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user1.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user2.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user3.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user4.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				<li>
					<img src="images/user5.png"/>
					<h4>Name Lastname</h4>
					<h5>Chief Creative Officer</h5>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<a href="#" class="p-link">Personal Website</a>
				</li>
				
			</ul>
		</div> -->
	</section>
	<section>
		<h1 class="db-font about-you"><span>It's not About Us.</span> Its About You!</h1>
		<div class="inner-container who-we-are">
			<div class="left-section">
					<h3>Who we are</h3>
					<p>We are a collective of 18 entrepreneurs, designers, developers, and thinkers. Our objective is to help you grow your business on all touch points. Whether it's internal operations, thinking through a new process, attracting new customers, creating a better social presence, or building a product or service, we'll help you attain your goals. Our Collective has over 100 years of combined experience in consulting, development, marketing and creative. We've helped over 150 businesses, and more than 15 Fortune 100 & 500 Companies. Our promise is to help you grow... Because if you don't, we don't.</p>
					<div class="why-we">
						<h3>Why we do it</h3>
						<p>Our inspiration is derived from yours. What makes DB Collective Inc. uniquely different is our team. Each of us has a specialty, a talent that is used to help people achieve exactly what our customers want. Located in Manhattan NY, Miami, Chicago and LA, we are in perfect urban settings that bring life to creativity and amazing product development.</p>
					</div>	
					
			</div>		
			<div class="right-section">		
					<h3>Technology that works for you</h3>
					<p>Our individual skills and extensive knowledge on the latest modern technology ensures that all of our websites are made to effectively and captivatingly inform people, all through three formulated steps: Design, Build, and Market. We start off by getting to know you and your business; finding out what is necessary to make you grow, how we can improve your ratings, and increase sales. You are the center of our focus, and we work to give you a variety of options to choose from. Then, we like to get tech-savy and creative using advanced technologies such as, HTML5, CSS3, JavaScript, jQuery, Angular, PHP, mySQL, WordPress, Git, Laravel, and state of the art servers to bring you the highest quality professional appearance you can get. It's all about stability and effort.</p>
					<div class="why-we">
						<h3>Invite people to your party</h3>
						<p>To seal it off, we take it to the best part, marketing your finished website, and thoughtfully promoting it. Advertisement is the name of the game. We have many successful campaigns in all of the major online advertising mediums, including: Google Adwords, Facebook, Linkedin and more. However the final product here is your successful business.</p>
					</div>	
			</div>		
		</div>
		<p class="our-promise"><span>Building a business is a big step in improving your way of life, and the lives of <br>those around you. Let us be your partner in doing so.</span> That is our promise.</p>
	</section>
	
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	
	</div>
 <script>
		$(document).ready(function(){
			 $('.bxslider').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 8,
		maxSlides: 8,
		
		slideWidth: 0,

			  mode: 'vertical',
			  responsive: true
			});
			//Menu Slide Js
	jQuery(document).ready(function($) {
		$('.smobitrigger').smplmnu();
		});
		});
</script>
	
</body>
</html>
