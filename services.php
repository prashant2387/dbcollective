<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title>Case Studies</title>

</head>
<body>
	<div class="wrapper" id="about">
	<header>
		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
			<?php include("header.php"); ?>
		</div>
	</header>
	
	<section class="case">
		<div class="case-title">
			<h1 class="db-font case-s">Services</h1>
		</div>	
		<div class="container grid-four">
			<div class="inner-container">
				<div class="servicess">
					<div class="width-40">
						<h2>Business <br>Development</h2>
					</div>
					<div class="width-20">
						<h3>Business <br>Consulting</h3>
						<p>Need us to be the thinkers? We can! We'll create better workflows, improve software & operations, or build your next million dollar idea.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
					<div class="width-20">
						<h3>Application <br>Development</h3>
						<p>We can build custom applications for web or mobile use. Have an idea you need to get off the ground? We'll scope it out and build it.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
					<div class="width-20">
						<h3>Investment <br>Strategies</h3>
						<p>Need to get funding? We can figure out if you should grow organically or raise funds to scale. We can work with you through the whole process.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
				</div>
				<div class="servicess">
					<div class="width-40">
						<h2>Web <br>Design</h2>
					</div>
					<div class="width-20">
						<h3>Websites</h3>
						<p>Need us to be the thinkers? We can! We'll create better workflows, improve software & operations, or build your next million dollar idea.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
					<div class="width-20">
						<h3>Landing <br>Pages</h3>
						<p>We can build custom applications for web or mobile use. Have an idea you need to get off the ground? We'll scope it out and build it.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
					<div class="width-20">
						<h3>Mobile <br>Apps</h3>
						<p>Need to get funding? We can figure out if you should grow organically or raise funds to scale. We can work with you through the whole process.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
				</div>
				<div class="servicess">
					<div class="width-40">
						<h2>Internet <br>Marketing</h2>
					</div>
					<div class="width-20">
						<h3>Search Engine <br>Optimization</h3>
						<p>Imagine being on the first page of all the major search engines for the keywords that your ideal customer is searching for. Yea, we can do that.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
					<div class="width-20">
						<h3>Pay-per <br>-Click</h3>
						<p>We can build custom applications for web or mobile use. Have an idea you need to get off the ground? We'll scope it out and build it.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
					<div class="width-20">
						<h3>Social Media <br>Marketing</h3>
						<p>Need to get funding? We can figure out if you should grow organically or raise funds to scale. We can work with you through the whole process.</p>
						<a href="#">Learn more <img src="images/right-arrow.png"/></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	</div>
 <script>
		$(document).ready(function(){
			 $('.bxslider').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 8,
		maxSlides: 8,
		
		slideWidth: 0,

			  mode: 'vertical',
			  responsive: true
			});
			//Menu Slide Js
	jQuery(document).ready(function($) {
		$('.smobitrigger').smplmnu();
		});
		});
</script>
	
</body>
</html>
