<?php get_header(); ?>
<script>
	jQuery(window).load(function() {
		var selecetd_taxonomy = jQuery("#standard-dropdown").val();
		jQuery('.tagged-posts').fadeOut();
		data = {
			action: 'filter_posts',
			afp_nonce:  jQuery('#afp_nonce').val(),
			taxonomy: selecetd_taxonomy,
		};
		jQuery.ajax({
			type: 'post',
			dataType: 'html',
			url: '<?php bloginfo('stylesheet_directory'); ?>/getdata.php',
			data: data,
			success: function( data, textStatus, XMLHttpRequest ) {
				jQuery('.work-grid').html( data );
				jQuery('.work-grid').fadeIn();
				//console.log( textStatus );
				//console.log( XMLHttpRequest );
			},
			error: function( MLHttpRequest, textStatus, errorThrown ) {
			//	console.log( MLHttpRequest );
			//	console.log( textStatus );
			//	console.log( errorThrown );
				jQuery('.work-grid').html( 'No posts found' );
				jQuery('.work-grid').fadeIn();
			}
		})
	});
		
		
	jQuery(document).ready(function(jQuery) {
		jQuery('#standard-dropdown').live('change',function(event){
			if (event.preventDefault) {
				event.preventDefault();
			} else {
				event.returnValue = false;
			}
			var selecetd_taxonomy = jQuery(this).val();
			jQuery('.tagged-posts').fadeOut();
			data = {
				action: 'filter_posts',
				afp_nonce:  jQuery('#afp_nonce').val(),
				taxonomy: selecetd_taxonomy,
			};
			jQuery.ajax({
				type: 'post',
				dataType: 'html',
				url: '<?php bloginfo('stylesheet_directory'); ?>/getdata.php',
				data: data,
				success: function( data, textStatus, XMLHttpRequest ) {
					jQuery('.work-grid').html( data );
					jQuery('.work-grid').fadeIn();
					//console.log( textStatus );
					//console.log( XMLHttpRequest );
				},
				error: function( MLHttpRequest, textStatus, errorThrown ) {
					//console.log( MLHttpRequest );
				//	console.log( textStatus );
					//console.log( errorThrown );
					jQuery('.work-grid').html( 'No posts found' );
					jQuery('.work-grid').fadeIn();
				}
			})
		});		
	});

</script>

<section class="case service-dev workk">
		<div class="case-title">
			<h1 class="db-font case-s">Work <span>/</span> </h1>
			 <p>
				<input type="hidden" value="<?php  echo wp_create_nonce( 'afp_nonce' ); ?>" name="afp_nonce" id="afp_nonce">
				<select id="standard-dropdown" name="standard-dropdown" class="custom-class1 custom-class2" style="width: 200px;">
						<option value='0' class='test-class-1'>All Projects</option>
				<?php $terms = get_terms('workcategory');
						foreach ( $terms as $term ) {
						  echo  "<option value='".$term->term_id."' class='test-class-1'>".$term->name."</option>";
						}
				?>
				</select>
			</p>
		</div>	

		<div class="container">
			<div class="work-grid">
			</div>
		</div>
</section>
	<section>
		<div class="container">
			<div class="inner-container">
				<footer>
				<?php include("footer.php"); ?>
					<!--small>&copy; 2016 DB Collective, Inc.</small>
					<a href="home.html">DB Collective</a-->
				</footer>
			</div>
		</div>
	</section>
	</div>

  <script type="text/javascript">

        $(document).ready(function () {

            $('#create').click(function () {
                $('SELECT').selectBox();
            });

            $('#destroy').click(function () {
                $('SELECT').selectBox('destroy');
            });

            $('#enable').click(function () {
                $('SELECT').selectBox('enable');
            });

            $('#disable').click(function () {
                $('SELECT').selectBox('disable');
            });

            $('#serialize').click(function () {
                $('#console').append('<br />-- Serialized data --<br />' +
                        $('FORM').serialize().replace(/&/g, '<br />') + '<br /><br />');
                $('#console')[0].scrollTop = $('#console')[0].scrollHeight;
            });

            $('#refresh').click(function() {
                $('SELECT OPTION').each(function() {
                    $(this).text('Refreshed ' + $(this).val());
                });
                $('SELECT').selectBox('refresh');
            });

            $('#value-1').click(function () {
                $('SELECT').selectBox('value', 1);
            });

            $('#value-2').click(function () {
                $('SELECT').selectBox('value', 2);
            });

            $('#value-2-4').click(function () {
                $('SELECT').selectBox('value', [2, 4]);
            });

            $('#options').click(function () {
                $('SELECT').selectBox('options', {
                    'Opt Group 1': {
                        '1': 'Value 1',
                        '2': 'Value 2',
                        '3': 'Value 3',
                        '4': 'Value 4',
                        '5': 'Value 5'
                    },
                    'Opt Group 2': {
                        '6': 'Value 6',
                        '7': 'Value 7',
                        '8': 'Value 8',
                        '9': 'Value 9',
                        '10': 'Value 10'
                    },
                    'Opt Group 3': {
                        '11': 'Value 11',
                        '12': 'Value 12',
                        '13': 'Value 13',
                        '14': 'Value 14',
                        '15': 'Value 15'
                    }
                });
            });

            $('#default').click(function () {
                $('SELECT').selectBox('settings', {
                    'menuTransition': 'default',
                    'menuSpeed': 0
                });
            });

            $('#fade').click(function () {
                $('SELECT').selectBox('settings', {
                    'menuTransition': 'fade',
                    'menuSpeed': 'fast'
                });
            });

            $('#slide').click(function () {
                $('SELECT').selectBox('settings', {
                    'menuTransition': 'slide',
                    'menuSpeed': 'fast'
                });
            });

            $('select')
				.selectBox({
					mobile: true
				})
				/*.focus(function () {
					$('#console').append('Focus on ' + $(this).attr('name') + '<br />');
					$('#console')[0].scrollTop = $('#console')[0].scrollHeight;
				})
				.blur(function () {
					$('#console').append('Blur on ' + $(this).attr('name') + '<br />');
					$('#console')[0].scrollTop = $('#console')[0].scrollHeight;
				})
				.change(function () {
					$('#console').append('Change on ' + $(this).attr('name') + ': ' + $(this).val() + '<br />');
					$('#console')[0].scrollTop = $('#console')[0].scrollHeight;
				});*/
        });

    </script>
</body>
</html>