<?php
define('WP_USE_THEMES', false);  
require_once('../../../wp-load.php');
global $post;
  if( !isset( $_POST['afp_nonce'] ) )
    die('Permission denied');
  $taxonomy = $_POST['taxonomy'];
  
	  if($taxonomy == 0){
			$args = array(
				'post_type' => 'work',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'order' => 'ASC'
			);
		$query = new WP_Query( $args );
	  }else{
		$args = array(
			'post_type' => 'work',
			'post_status' => 'publish',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'workcategory',
				   'terms' => $taxonomy, 
				)
			)
		);
		  if( !$taxonomy ) {
			unset( $args['tag'] );
		  }
		$query = new WP_Query( $args );
	}
?>
  <ul class="video_link">
  <?php
	  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
	  <!----------------------------------------->
			<?php
				$id = get_the_ID();
				$link = get_permalink($id);
				$post = get_post($id);
			?>
				<li><a data-toggle="modal" data-target=".bs-example-modal-lg<?php echo $id; ?>" href="#">
				<?php if ( has_post_thumbnail() ) : ?>
					<img src="<?php the_post_thumbnail_url(); ?>"/>
				<?php endif; ?>
				</a>

			<!-----------------0th------------------------->
				<div class="modal fade bs-example-modal-lg<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  <div class="modal-dialog work-outer">
					<div class="modal-content work-popup">
					<a class="close cross-right" data-dismiss="modal" href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/close-icon.png"/></a>
					  <div id="carousel-example-generic<?php echo $id; ?>" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
						<!-- Wrapper for slides -->
							  <div class="carousel-inner">
									<?php 
										$i=1;
										$imgFiles = get_post_meta( $id, 'imgupload', true );
										foreach($imgFiles as $key => $audioFile){							
										$get_the_guid = get_the_guid($audioFile['gallery-image']);
									?>
										<?php if($i == 1){ echo "<div class='item active'>"; } else { echo "<div class='item '>";  } ?>
												<div style="float:left;width:100%;background-repeat:no-repeat;background-size:cover;">
													<img src="<?php echo $get_the_guid; ?>">
												</div>
											</div>
										<?php $i++;  }  ?>
										</div>

						  <!-- Controls -->
							<div class="work-info">
								<h1><?php the_title(); ?></h1>
									<h6>project category</h6>
									<div class="accordion">
									<h1>about the company</h1>
									<div><?php echo get_post_meta($id, 'about-the-company', true); ?></div>
									<h1>problems</h1>
									<div><?php echo get_post_meta($id, 'workproblems', true); ?></div>
									<h1>Solutions</h1>
									<div><?php echo get_post_meta($id, 'worksolutions', true); ?></div>
									<h1>results</h1>
									<div><?php echo get_post_meta($id, 'workresults', true); ?></div>
									</div>
									<a href="<?php echo get_post_meta($id, 'website-url', true); ?>">Go to the website 
									<span class="white-arrow"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/arrow-white.png"/></span></a>
							</div>
							
							<ol class="carousel-indicators">
								<?php 
									for($i=0;$i< count($imgFiles); $i++){
										if($i == 0){
											echo '<li data-target="#carousel-example-generic'.$id.'" data-slide-to="'.$i.'" class="active"></li>';
										}else{
											echo '<li data-target="#carousel-example-generic'.$id.'" data-slide-to="'.$i.'" class=""></li>';
										}
										
									}
								?>
							</ol>
							

						</div>
					</div>
				</div>
			</div>
		</li>	
	<!-------------------------------------------->
  <?php endwhile;  wp_reset_query(); ?>
  </ul>
  	 <script>
		$(document).ready(function(){
			var mySlider = $('.bxslider').bxSlider({
			  auto:true,
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 8,
			  maxSlides: 8,
			  slideWidth: 0,
			  mode: 'vertical',
			  responsive: true
			});

			$("#slidemain").click(function(){
				$(".sliderPopUp").toggle();
				mySlider.reloadSlider();
				$('.slider-main').bxSlider({
			  auto:false,
			  minSlides: 1,
			  maxSlides: 1,
			  slideWidth: 0,
			  responsive: true
			});
			});
			//Menu Slide Js
			$('.smobitrigger').smplmnu();
		});
		
		
		$(document).ready( function() {
	    $(".label_better").label_better({
	      easing: "bounce"
	    });
	  });
	$(".accordion").accordion();
</script>
  <?php else: ?>
    <h2>No posts found</h2>
  <?php endif;
  die();
  ?>