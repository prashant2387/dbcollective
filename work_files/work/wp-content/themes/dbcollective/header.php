<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    
   	<!--Fonts-->
	<title>Work</title>
    
   	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/simpleMobileMenu.css">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.bxslider.css" rel="stylesheet" />
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/woco-accordion.min.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/work-style.css" rel="stylesheet" />
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.selectBox.css"/>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom-bootstrap.css">
	
		
	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/simpleMobileMenu.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.selectBox.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/html5.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.label_better.js"></script>
	<!--<script type="text/javascript" src="js/jquery.colorbox.js"></script>-->
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/woco.accordion.min.js"></script>

</head>
<body>
	<div class="wrapper" id="about">
	<header>
		<!--a href="home.html" class="logo"><img src="images/logo.jpg"/></a-->
		<?php twentysixteen_the_custom_logo(); ?>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
			<ul class="mobimenu">
				<li><a href="">Work</a></li>
				<li><a href="case-studies.php">Case Studies</a></li>
				<li><a href="services.php">Services</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
				<li class="ballon-relative"><a href="#">Blog<span class="ballon"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/ballon.png"/></span></a></li>
			</ul>
		</div>
	</header>