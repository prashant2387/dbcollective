<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<!--css-->
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/simpleMobileMenu.css">
	<link href="css/mouseparallax.css" rel="stylesheet" />
	<link href="css/jquery.bxslider.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/fakeLoader.css">

	<!--js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	
	<script src="js/fakeLoader.js"></script>
	 <script>
			$(document).ready(function(){
			
				//$('body').css('overflow-y','hidden');
				$(".fakeloader").fakeLoader({
					timeToHide:15000,
					bgColor:"rgba(0, 0, 0, 0.96)",
					spinner:"spinner6"
				});
				
				
			});
	  </script>
	
	
	<script src="js/typed.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/simpleMobileMenu.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> -->
	<script type="text/javascript" src="js/html5.js"></script>
	<script src="https://use.typekit.net/cdk5xxk.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="icon" href="images/fav.ico" type="image/ico">
	<!--Fonts-->
	<title>DB Collective</title>

</head>


<div class="loader-message">
<div class="ld-wrap">
<div class="type-wrap">
    <div id="typed-strings">
        <span>Hi, we are a Creative Digital Agency :)</span>
    </div>
    <span id="typed" style="white-space:pre;"></span>
</div>
</div>
</div>



<div id="welcome" class="m-popup">
<div class="tbl-one">
    <a id="close" class="close-cls" href="javascript:void(0)"><img src="images/close-icon.png" alt=""></a>
	<div class="info-popup">
    <h1>Hello there.<br> Are you interested in web, <br> mobile, or creative services?</h1>
	<div class="form-submit">
		<input value="enter your email" onfocus="if(this.value=='enter your email')this.value='';" onblur="if(this.value=='')this.value='enter your email';" type="text">
		<a class="cnt" href="javascript:void(0)">contact us</a>
	</div>
	</div>
	</div>
</div>

<body>
	<div class="wrapper wrap push">
	<div class="snap-slide">
	<header>
		<a href="/" class="logo"><img src="images/logo.jpg"/></a>
		<div class="navigation">
		<a href="javascript:void(0)" class="smobitrigger ion-navicon-round"><span>Menu</span></a>
	<?php include("header.php"); ?>
		</div>
	</header>
	<div class="slider-main">
		<ul class="bxslider">
		  <li><img src="images/slide1.jpg" />
			<div class="slidertext">
				<h1>Name of the Project</h1>
				<a href="#">See case study <span class=""><img src="images/right-arrow.png"/></span></a>
			</div>
		  </li>
		  <li><img src="images/slide2.jpg" />
			<div class="slidertext sld-heading">
				<h1>Name of the Project</h1>
				<a href="#">See case study <span class=""><img src="images/right-arrow-w.png"/></span></a>
			</div>
		  </li>
		  <li><img src="images/slide3.jpg" />
			<div class="slidertext sld-heading2">
				<h1>Name of the Project</h1>
				<a href="#">See case study <span><img src="images/right-arrow-w.png"/></span></a>
			</div>
		  </li>
		</ul>
	</div>
	
	<section>
		<div class="container grid-four">
			<div class="out-main">
				<div class="left-sec">
					
			<div class="inner-section">
				<div class="perla">
					<div id="perla-hover" class="hover-container mouse-bg"><img src="images/perla-hover.png"/> </div>
					<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
				<div class="umja">
					<div id="umja-hover" class="hover-container mouse-bg"><img src="images/umja-hover.png"/> </div>
					<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
				</div>
			</div>
			<div class="inner-section gains">
				<div id="gains-hover" class="hover-container mouse-bg"><img src="images/gains-hover.png"/> </div>
				<a href="#">See Project <span><img src="images/white-arrow.png"/></span></a>
			</div>
				</div>
				<div class="right-sec">
					<div class="inner-section clients" id="client">
				<h3>we've worked with</h3>
				<ul class="bxslider2">
					<li><div><img src="images/united-nation.png"/></div><div><img src="images/you.png"/></div></li>
					<li><div><img src="images/quality.png"/></div><div><img src="images/ford.png"/></div></li>
					<li><div><img src="images/memorial.png"/></div><div><img src="images/nhl.png"/></div></li>
					<li><div><img src="images/atnt.png"/></div><div><img src="images/wyndam.png"/></div></li>
					<li><div><img src="images/scholastic.png"/></div><div><img src="images/m-bens.png"/></div></li>
					<li><div><img src="images/polo.png"/></div><div><img src="images/trimble.png"/></div></li>
					<li><div><img src="images/holidayinn.png"/></div><div><img src="images/state.png"/></div></li>
					<li><div><img src="images/handmade.png"/></div><div><img src="images/chandon.png"/></div></li>
					<li><div><img src="images/united-nation.png"/></div><div><img src="images/you.png"/></div></li>
					<li><div><img src="images/quality.png"/></div><div><img src="images/ford.png"/></div></li>
					<li><div><img src="images/memorial.png"/></div><div><img src="images/nhl.png"/></div></li>
					<li><div><img src="images/atnt.png"/></div><div><img src="images/wyndam.png"/></div></li>
					<li><div><img src="images/scholastic.png"/></div><div><img src="images/m-bens.png"/></div></li>
					<li><div><img src="images/polo.png"/></div><div><img src="images/trimble.png"/></div></li>
					<li><div><img src="images/holidayinn.png"/></div><div><img src="images/state.png"/></div></li>
					<li><div><img src="images/handmade.png"/></div><div><img src="images/chandon.png"/></div></li>
					<li><div><img src="images/united-nation.png"/></div><div><img src="images/you.png"/></div></li>
					<li><div><img src="images/quality.png"/></div><div><img src="images/ford.png"/></div></li>
					<li><div><img src="images/memorial.png"/></div><div><img src="images/nhl.png"/></div></li>
					<li><div><img src="images/atnt.png"/></div><div><img src="images/wyndam.png"/></div></li>
				</ul>
			</div>
				</div>
			</div>
			</div>
	</section>
	</div>
	<section class="info-add">
		<div class="container">
			<div class="inner-container">
			
			    <div class="full-about">
				<div class="about">
						<h2>We are DB Collective :)</h2>
						<p>We're a Collective of 18 entrepreneurs, designers, developers, and thinkers. Our objective is to help you grow on all touch points. Whether it's internal operations, thinking through a new process, attracting new customers, creating a better social presence, or building a product or service, we'll help you attain your goals. Our Collective has over 100 years of combined experience in consulting, development, marketing and creative. We've helped over 150 businesses, and more than 15 Fortune 100 & 500 Companies. Our promise is to help you grow... Because if you don't, we don't.</p>
					</div>
				</div>
			
				<div class="info">
					
					<div class="contact">
						<h2>Interested in our services?<br> Drop us a line and we will be<br> in contact with you shortly.</h2>
						<!--<form method="post">
							<label>Full Name <span>*</span></label>
							<input type="text" name="fullname" placeholder="" required /><br>
							<label>Email <span>*</span></label>
							<input type="email" name="email" placeholder="" required /><br>
							<label>Phone Number <span>*</span></label>
							<input type="text" name="phone" placeholder="" pattern="[0-9]+" title="only 0-9 digits" required /><br>
							<label>Details <span>*</span></label>
							<textarea name="description" placeholder="" required /></textarea>
							<div class="clear"></div>
							<input type="submit" name="submit" value="Send" class="btn-send b-send">
						</form-->
					</div>
					
				<div class="in-part">
					<form method="post">
						<div class="new-contact">
							<input name="fullname" placeholder="Full Name *" class="invt inradius" type="text" required>
							<input name="email" placeholder="Email *"  class="invt" type="text" required>
							<input name="phone" placeholder="Phone Number *"  class="invt" type="text" required>
						<textarea name="description" class="input_txtarea" placeholder="Details *" required></textarea>
						</div>
						<div class="bt-bg-send">
						<input type="submit" name="submit" value="Contact Us" class="n-send">
						</div>
					</form>
				</div>	
			<?php
				if(isset($_POST['submit'])){
					$fullname = $_POST['fullname'];
					$email = $_POST['email'];
					$phone = $_POST['phone'];
					$description = $_POST['description'];
					
					$to = "design@dbcollective.co"; 
					$subject = $fullname." has contacted you through dbcollective";
					$from = $email;
		
					$message = "<html>";
					$message .= "<head>
					<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
					<title>email-template</title>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
					</head>";
					$message .= "<body style='margin:0; padding:0; font-family:helvetica'>";
					$message .= " <table style='width:100%; max-width:600px; margin:0 auto;' border='1' cellpadding='0' cellspacing='0'><tr><td>";
					$message .= " <table width='100%' cellpadding='0' cellspacing='0'>
										  <tr>
											  <td style='padding-left:8px; background:#000000; color:#ffffff; padding-top:10px; padding-bottom:6px; border-bottom:25px solid #000000; text-align:center;'>You have received a new message from the contact form on your website</td>
										  </tr>	  
									</table>";
						$message .= "<table width='100%' style='padding:0 10px;'>
						 <tr>
							  <td>	
								<p style='font-size:12px;'><strong>Name : </strong>".$fullname."</p>											  
								 <p style='font-size:12px;'><strong>Email : </strong>".$email."</p>										 
								 <p style='font-size:12px;'><strong>Phone : </strong> ".$phone."</p>
								 <p style='margin-bottom:40px;font-size:12px;'><strong>Description :</strong> ".$description."</p>
							  </td>

						  </tr>
					</table>";
					$message .= "</td></tr></table></body></html>";							
					$headers ="MIME-Version: 1.0" . "\r\n";
					$headers .="Content-type:text/html;charset=iso-8859-1" . "\r\n";
					$headers .="From: $from" . "\r\n";

					
					$mail = mail($to, $subject, $message, $headers);
					if($mail){ echo "<p class='success'><span>Thankyou! </span>Your message sent successfully.</p>"; }
					else{ echo "<p class='form_error'>Sorry! Some error occured.</p>";}
				}
			?>
					
			
				
				</div>
				<div class="info addresss">
					<h6>ADRESSES</h6>
					<address>
						<b>Fort Lauderdale</b><br>
						100 N Federal Highway, Suite #C4,<br> 
						Fort Lauderdale, FL 33301

					</address>
					<address>
						<b>Manhattan</b><br>
						300 W 55TH Street, <br>
						Suite 17L, New York 10019<br>
					</address>
					<div class="contact-info">
						<h6>PHONE</h6>
						<a href="#">(888) 233-2383</a>
					</div>
					<div class="contact-info">
						<h6>EMAILS</h6>
						<a href="#">info@dbcollective.co</a>
					</div>
					<div class="contact-info">
						<h6>OFFICE HOURS</h6>
						<p>9 AM - 6 PM Mon - Fri</p>
					</div>
					<div class="social">
						<ul>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="fb"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	<footer class="footer-outer">
	    <div class="inner-container">
			<?php include("footer.php"); ?>
		</div>	
	</footer>
	</div>
	  <!-- SnapScroll Core Files -->
  <script type="text/JavaScript" src="js/jquery.scroll_to.js"></script>
  <script type="text/JavaScript" src="js/jquery.snapscroll.min.js"></script>
  <script type="text/JavaScript" src="js/mouse.parallax.js"></script>
 <script>
		$(document).ready(function(){
			 $('.bxslider').bxSlider({
			  auto:true,
			  mode: 'fade',
			  minSlides: 1,
			  moveSlides: 1,
			  responsive: true
			});
			$('.bxslider2').bxSlider({
			  auto:true,
			  minSlides: 6,
		maxSlides: 6,
		
		slideWidth: 0,
			 moveSlides: 2,
			  mode: 'vertical',
			  responsive: true
			});
			//Menu Slide Js
	jQuery(document).ready(function($) {
		$('.smobitrigger').smplmnu();
		});
		});
</script>
<script>
	$(function() {
	  $(".snap-slide").snapscroll();
	}); 
</script>	
<script>
	$(document).ready(function() {
		$('#perla-hover').mouseParallax({ moveFactor: 4 });
		$('#umja-hover').mouseParallax({ moveFactor: 4 });
		$('#gains-hover').mouseParallax({ moveFactor: 4 });
	});
</script>

<script>
	$(window).load(function(){
			
			var slide = function () {
                //$("#welcome").slideDown(800, 'swing');
            }
            window.setTimeout(slide, 2000);
			
			$('#close').on('click', function(e) { 				
				$(".wrapper").css("overflow","visible"); 
				$('#welcome').slideUp(800, 'swing')(); 								
            });
			
	});
</script>


<script>
    $(function(){

        $("#typed").typed({
            // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
            stringsElement: $('#typed-strings'),
            typeSpeed: 80,
            backDelay: 500,
            loop: false,
            contentType: 'html', // or text
            // defaults to false for infinite loop
            loopCount: false,
            callback: function(){ foo(); },
            resetCallback: function() { newTyped(); }
        });

        $(".reset").click(function(){
            $("#typed").typed('reset');
        });

    });

    function newTyped(){ /* A new typed object */ }

    function foo(){ console.log("Callback"); }

</script>
<script>
$(document).ready(function(){
	$("#welcome").css('opacity','0');
	$(".wrapper").css('opacity','0');
	$(".loader-message").css('overflow','auto');
	setTimeout(function() {				
		$(".wrapper").css('opacity','1');			
		$("#welcome").css('opacity','1');
		$(".wrapper").css('overflow','visible');
		setTimeout('showPopup()',2000);
		$("#welcome").css('overflow','auto');
		$(".loader-message").hide();		 
	}, 6000);
});
function showPopup() {
	$("#welcome").slideDown(800, 'swing');
	if($('#welcome').css('display')=='block') {
		$(".wrapper").css('overflow','hidden');		
	}
}
if($(".loader-message").css('display')=='block') {
	$(".wrapper").css('overflow','hidden');
}
</script>

</body>
</html>
